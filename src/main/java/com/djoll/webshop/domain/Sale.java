package com.djoll.webshop.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer quantity;
    private Double total;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Product product;

}
