package com.djoll.webshop.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
public class CustomerCommand {
    private Long id;

    @NotBlank
    @Size(min = 2, max = 255)
    private String name;

    @Email
    @NotBlank
    private String email;

    private Double total;
}
