package com.djoll.webshop.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

@Setter
@Getter
@NoArgsConstructor
public class ProductCommand {
    private Long id;

    @NotBlank
    @Size(min = 2, max = 255)
    private String name;

    @Min(0)
    @Max(99999)
    @NotNull
    private Integer quantity;

    @Min(0)
    @NotNull
    private Double price;
}
