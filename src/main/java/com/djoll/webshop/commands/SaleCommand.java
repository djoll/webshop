package com.djoll.webshop.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SaleCommand {
    private Long id;

    private Integer quantity;

    private Double total;
    private CustomerCommand customer;
    private ProductCommand product;

}
