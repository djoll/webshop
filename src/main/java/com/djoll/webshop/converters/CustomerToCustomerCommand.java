package com.djoll.webshop.converters;

import com.djoll.webshop.commands.CustomerCommand;
import com.djoll.webshop.domain.Customer;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerCommand implements Converter<Customer, CustomerCommand> {

    @Override
    @Nullable
    @Synchronized
    public CustomerCommand convert(Customer customer) {

        if (customer == null) {
            return null;
        }

        final CustomerCommand customerCommand = new CustomerCommand();
        customerCommand.setId(customer.getId());
        customerCommand.setName(customer.getName());
        customerCommand.setEmail(customer.getEmail());
        customerCommand.setTotal(customer.getTotal());
        return customerCommand;
    }
}
