package com.djoll.webshop.converters;

import com.djoll.webshop.commands.ProductCommand;
import com.djoll.webshop.domain.Product;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ProductCommandToProduct implements Converter<ProductCommand, Product> {

    @Nullable
    @Synchronized
    @Override
    public Product convert(ProductCommand input) {

        if (input == null) {
            return null;
        }

        final Product output = new Product();
        output.setId(input.getId());
        output.setName(input.getName());
        output.setPrice(input.getPrice());
        output.setQuantity(input.getQuantity());
        return output;
    }
}
