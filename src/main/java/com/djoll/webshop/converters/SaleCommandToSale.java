package com.djoll.webshop.converters;

import com.djoll.webshop.commands.SaleCommand;
import com.djoll.webshop.domain.Sale;
import lombok.AllArgsConstructor;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SaleCommandToSale implements Converter<SaleCommand, Sale> {

    private final CustomerCommandToCustomer customerConverter;
    private final ProductCommandToProduct productConverter;

    @Nullable
    @Synchronized
    @Override
    public Sale convert(SaleCommand input) {

        if (input == null) {
            return null;
        }

        Sale output = new Sale();
        output.setId(input.getId());
        output.setProduct(productConverter.convert(input.getProduct()));
        output.setCustomer(customerConverter.convert(input.getCustomer()));
        output.setQuantity(input.getQuantity());
        output.setTotal(input.getTotal());
        return output;
    }
}
