package com.djoll.webshop.converters;

import com.djoll.webshop.commands.ProductCommand;
import com.djoll.webshop.domain.Product;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ProductToProductCommand implements Converter<Product, ProductCommand> {

    @Override
    @Nullable
    @Synchronized
    public ProductCommand convert(Product input) {

        if (input == null) {
            return null;
        }

        final ProductCommand output = new ProductCommand();
        output.setId(input.getId());
        output.setName(input.getName());
        output.setPrice(input.getPrice());
        output.setQuantity(input.getQuantity());
        return output;
    }
}
