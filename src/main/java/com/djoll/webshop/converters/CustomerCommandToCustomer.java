package com.djoll.webshop.converters;

import com.djoll.webshop.commands.CustomerCommand;
import com.djoll.webshop.domain.Customer;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CustomerCommandToCustomer implements Converter<CustomerCommand, Customer> {

    @Override
    @Nullable
    @Synchronized
    public Customer convert(CustomerCommand customerCommand) {

        if (customerCommand == null) {
            return null;
        }

        final Customer customer = new Customer();
        customer.setId(customerCommand.getId());
        customer.setName(customerCommand.getName());
        customer.setEmail(customerCommand.getEmail());
        customer.setTotal(customerCommand.getTotal());
        return customer;
    }
}
