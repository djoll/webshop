package com.djoll.webshop.converters;

import com.djoll.webshop.commands.SaleCommand;
import com.djoll.webshop.domain.Sale;
import lombok.AllArgsConstructor;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SaleToSaleCommand implements Converter<Sale, SaleCommand> {

    private final CustomerToCustomerCommand customerConverter;
    private final ProductToProductCommand productConverter;

    @Nullable
    @Synchronized
    @Override
    public SaleCommand convert(Sale input) {

        if (input == null) {
            return null;
        }

        SaleCommand output = new SaleCommand();
        output.setId(input.getId());
        output.setProduct(productConverter.convert(input.getProduct()));
        output.setCustomer(customerConverter.convert(input.getCustomer()));
        output.setQuantity(input.getQuantity());
        output.setTotal(input.getTotal());
        return output;
    }
}
