package com.djoll.webshop.services;

import com.djoll.webshop.commands.CustomerCommand;
import com.djoll.webshop.converters.CustomerCommandToCustomer;
import com.djoll.webshop.converters.CustomerToCustomerCommand;
import com.djoll.webshop.domain.Customer;
import com.djoll.webshop.exceptions.NotFoundException;
import com.djoll.webshop.repositories.CustomerRepository;
import com.djoll.webshop.repositories.SaleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final SaleRepository saleRepository;
    private final CustomerCommandToCustomer customerCommandToCustomer;
    private final CustomerToCustomerCommand customerToCustomerCommand;

    @Override
    public Set<Customer> getCustomers() {

        Set<Customer> customerSet = new HashSet<>();
        customerRepository.findAll().iterator().forEachRemaining(customerSet::add);

        return customerSet;
    }

    @Transactional
    @Override
    public CustomerCommand saveCustomerCommand(CustomerCommand command) {
        Customer detachedCustomer = customerCommandToCustomer.convert(command);

        if (detachedCustomer.getTotal() == null) {
            detachedCustomer.setTotal(0D);
        }

        Customer savedCustomer = customerRepository.save(detachedCustomer);
        return customerToCustomerCommand.convert(savedCustomer);
    }

    @Transactional
    @Override
    public CustomerCommand findCommandById(Long id) {
        return customerToCustomerCommand.convert(findById(id));
    }

    @Override
    public Customer findById(Long id) {

        Optional<Customer> customerOptional = customerRepository.findById(id);

        if (!customerOptional.isPresent()) {
            throw new NotFoundException("Customer not found. For ID value: " + id.toString());
        }

        return customerOptional.get();
    }

    @Override
    public void deleteById(Long idToDelete) {

        saleRepository.findSalesByCustomer(customerRepository.findById(idToDelete).get()).iterator().forEachRemaining(saleRepository::delete);

        customerRepository.deleteById(idToDelete);

    }
}
