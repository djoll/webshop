package com.djoll.webshop.services;

import com.djoll.webshop.commands.ProductCommand;
import com.djoll.webshop.converters.ProductCommandToProduct;
import com.djoll.webshop.converters.ProductToProductCommand;
import com.djoll.webshop.domain.Product;
import com.djoll.webshop.exceptions.NotFoundException;
import com.djoll.webshop.repositories.ProductRepository;
import com.djoll.webshop.repositories.SaleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final SaleRepository saleRepository;
    private final ProductToProductCommand productToProductCommand;
    private final ProductCommandToProduct productCommandToProduct;

    @Override
    public Set<Product> getProducts() {

        Set<Product> productSet = new HashSet<>(); // avoiding a Null pointer
        productRepository.findAll().iterator().forEachRemaining(productSet::add);

        return productSet;
    }

    @Transactional
    @Override
    public ProductCommand saveProductCommand(ProductCommand command) {

        Product detachedProduct = productCommandToProduct.convert(command);

        Product savedProduct = productRepository.save(detachedProduct);

        return productToProductCommand.convert(savedProduct);
    }

    @Transactional
    @Override
    public ProductCommand findCommandById(Long id) {
        return productToProductCommand.convert(findById(id));
    }

    @Override
    public Product findById(Long id) {

        Optional<Product> productOptional = productRepository.findById(id);

        if (!productOptional.isPresent()) {
            throw new NotFoundException("Product not found. For ID value: " + id.toString());
        }

        return productOptional.get();
    }

    @Override
    public void deleteById(Long idToDelete) {

        saleRepository.findSalesByProduct(productRepository.findById(idToDelete).get()).iterator().forEachRemaining(saleRepository::delete);

        productRepository.deleteById(idToDelete);
    }
}
