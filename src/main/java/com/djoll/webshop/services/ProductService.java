package com.djoll.webshop.services;

import com.djoll.webshop.commands.ProductCommand;
import com.djoll.webshop.domain.Product;

import java.util.Set;

public interface ProductService {

    Set<Product> getProducts();

    ProductCommand saveProductCommand(ProductCommand command);

    ProductCommand findCommandById(Long id);

    Product findById(Long id);

    void deleteById(Long idToDelete);
}
