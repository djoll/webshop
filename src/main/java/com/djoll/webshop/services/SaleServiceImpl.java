package com.djoll.webshop.services;

import com.djoll.webshop.commands.SaleCommand;
import com.djoll.webshop.converters.SaleCommandToSale;
import com.djoll.webshop.converters.SaleToSaleCommand;
import com.djoll.webshop.domain.Customer;
import com.djoll.webshop.domain.Product;
import com.djoll.webshop.domain.Sale;
import com.djoll.webshop.repositories.CustomerRepository;
import com.djoll.webshop.repositories.ProductRepository;
import com.djoll.webshop.repositories.SaleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class SaleServiceImpl implements SaleService {

    private final SaleRepository saleRepository;
    private final SaleCommandToSale saleCommandToSale;
    private final SaleToSaleCommand saleToSaleCommand;
    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;

    @Override
    public Set<Sale> getSales() {

        Set<Sale> saleSet = new HashSet<>();
        saleRepository.findAll().iterator().forEachRemaining(saleSet::add);
        return saleSet;
    }

    @Transactional
    @Override
    public SaleCommand saveSaleCommand(SaleCommand command) {

        Sale detachedSale = saleCommandToSale.convert(command);

        Optional<Product> productOptional = productRepository.findById(detachedSale.getProduct().getId());
        Product product = productOptional.get();

        Optional<Customer> customerOptional = customerRepository.findById(detachedSale.getCustomer().getId());
        Customer customer = customerOptional.get();

        detachedSale.setTotal(command.getQuantity() * product.getPrice());
        product.setQuantity(product.getQuantity() - detachedSale.getQuantity());
        customer.setTotal(customer.getTotal() + detachedSale.getTotal());

        productRepository.save(product);
        customerRepository.save(customer);
        Sale savedSale = saleRepository.save(detachedSale);

        return saleToSaleCommand.convert(savedSale);
    }
}
