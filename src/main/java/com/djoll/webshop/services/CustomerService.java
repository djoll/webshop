package com.djoll.webshop.services;

import com.djoll.webshop.commands.CustomerCommand;
import com.djoll.webshop.domain.Customer;

import java.util.Set;

public interface CustomerService {

    Set<Customer> getCustomers();

    CustomerCommand saveCustomerCommand(CustomerCommand command);

    CustomerCommand findCommandById(Long id);

    Customer findById(Long id);

    void deleteById(Long idToDelete);

}
