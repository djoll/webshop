package com.djoll.webshop.services;

import com.djoll.webshop.commands.SaleCommand;
import com.djoll.webshop.domain.Sale;

import java.util.Set;

public interface SaleService {

    Set<Sale> getSales();

    SaleCommand saveSaleCommand(SaleCommand command);
}
