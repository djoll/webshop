package com.djoll.webshop.dbinitialization;

import com.djoll.webshop.domain.Customer;
import com.djoll.webshop.domain.Product;
import com.djoll.webshop.repositories.CustomerRepository;
import com.djoll.webshop.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DBInitialization implements ApplicationListener<ContextRefreshedEvent> {

    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (productRepository.count() == 0L) {
            loadProducts();
        }

        if (customerRepository.count() == 0L) {
            loadCustomers();
        }

    }

    private void loadCustomers() {

        Customer customer1 = new Customer();
        customer1.setName("John Doe");
        customer1.setEmail("johndoe@mail.com");
        customer1.setTotal(0D);
        customerRepository.save(customer1);

        Customer customer2 = new Customer();
        customer2.setName("Jane Roe");
        customer2.setEmail("janeroe@mail.com");
        customer2.setTotal(0D);
        customerRepository.save(customer2);

    }

    private void loadProducts() {

        Product product1 = new Product();
        product1.setName("IntelliJ IDEA Ultimate");
        product1.setPrice(499.00);
        product1.setQuantity(1000);
        productRepository.save(product1);

        Product product2 = new Product();
        product2.setName("CLion");
        product2.setPrice(199.00);
        product2.setQuantity(500);
        productRepository.save(product2);

        Product product3 = new Product();
        product3.setName("WebStorm");
        product3.setPrice(129.00);
        product3.setQuantity(3000);
        productRepository.save(product3);

    }

}
