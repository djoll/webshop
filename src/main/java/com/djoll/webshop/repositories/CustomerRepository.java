package com.djoll.webshop.repositories;

import com.djoll.webshop.domain.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
