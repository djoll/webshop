package com.djoll.webshop.repositories;

import com.djoll.webshop.domain.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
