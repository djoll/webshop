package com.djoll.webshop.repositories;

import com.djoll.webshop.domain.Customer;
import com.djoll.webshop.domain.Product;
import com.djoll.webshop.domain.Sale;
import org.springframework.data.repository.CrudRepository;

public interface SaleRepository extends CrudRepository<Sale, Long> {
    Iterable<Sale> findSalesByCustomer(Customer customer);
    Iterable<Sale> findSalesByProduct(Product product);
}
