package com.djoll.webshop.controllers;

import com.djoll.webshop.commands.ProductCommand;
import com.djoll.webshop.services.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("products")
    public String listProducts(Model model) {

        model.addAttribute("products", productService.getProducts());

        return "product/list";
    }

    @GetMapping("product/new")
    public String newProduct(Model model) {

        model.addAttribute("product", new ProductCommand());

        return "product/form";

    }

    @GetMapping("product/{id}/edit")
    public String editProduct(@PathVariable String id, Model model) {

        model.addAttribute("product", productService.findCommandById(Long.valueOf(id)));

        return "product/form";

    }

    @PostMapping("product")
    public String saveOrUpdate(@Valid @ModelAttribute("product")ProductCommand command, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "product/form";
        }

        productService.saveProductCommand(command);

        return "redirect:/products";
    }

    @GetMapping("product/{id}/delete")
    public String deleteProduct(@PathVariable String id) {

        productService.deleteById(Long.valueOf(id));

        return "redirect:/products";
    }

}
