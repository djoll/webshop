package com.djoll.webshop.controllers;

import com.djoll.webshop.commands.CustomerCommand;
import com.djoll.webshop.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("customers")
    public String listCustomers(Model model) {

        model.addAttribute("customers", customerService.getCustomers());

        return "customer/list";
    }

    @GetMapping("customer/new")
    public String newCustomer(Model model) {

        model.addAttribute("customer", new CustomerCommand());

        return "customer/form";
    }

    @GetMapping("customer/{id}/edit")
    public String editCustomer(@PathVariable String id, Model model) {

        model.addAttribute("customer", customerService.findCommandById(Long.valueOf(id)));

        return "customer/form";
    }

    @PostMapping("customer")
    public String saveOrUpdate(@Valid @ModelAttribute("customer")CustomerCommand customer, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "customer/form";
        }

        customerService.saveCustomerCommand(customer);

        return "redirect:/customers";
    }

    @GetMapping("customer/{id}/delete")
    public String deleteCustomer(@PathVariable String id) {

        customerService.deleteById(Long.valueOf(id));

        return "redirect:/customers";
    }

}
