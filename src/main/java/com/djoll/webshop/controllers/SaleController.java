package com.djoll.webshop.controllers;

import com.djoll.webshop.commands.CustomerCommand;
import com.djoll.webshop.commands.ProductCommand;
import com.djoll.webshop.commands.SaleCommand;
import com.djoll.webshop.domain.Product;
import com.djoll.webshop.services.CustomerService;
import com.djoll.webshop.services.ProductService;
import com.djoll.webshop.services.SaleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@AllArgsConstructor
public class SaleController {

    private final SaleService saleService;
    private final CustomerService customerService;
    private final ProductService productService;

    @GetMapping("sales")
    public String listProducts(Model model) {

        model.addAttribute("sales", saleService.getSales());

        return "sale/list";
    }

    @GetMapping("sale/new")
    public String newSale(Model model) {

        SaleCommand saleCommand = new SaleCommand();

        model.addAttribute("sale", saleCommand);

        saleCommand.setCustomer(new CustomerCommand());

        model.addAttribute("customers", customerService.getCustomers());

        saleCommand.setProduct(new ProductCommand());

        model.addAttribute("products", productService.getProducts());

        return "sale/form";

    }

    @PostMapping("sale")
    public String save(@ModelAttribute("sale")SaleCommand command) {

        Product product = productService.findById(command.getProduct().getId());

        if (product.getQuantity() >= command.getQuantity()) {
            saleService.saveSaleCommand(command);
            return "redirect:/sales";
        }
        else {
            return "redirect:/sales";
        }
    }

}
