create table customer (id bigint not null auto_increment, email varchar(255), name varchar(255), total double precision, primary key (id)) engine=InnoDB;
create table product (id bigint not null auto_increment, name varchar(255), price double precision, quantity integer, primary key (id)) engine=InnoDB;
create table sale (id bigint not null auto_increment, quantity integer, total double precision, customer_id bigint, product_id bigint, primary key (id)) engine=InnoDB;
alter table sale add constraint FKjw88ojfoqquyd9f1obip1ar0g foreign key (customer_id) references customer (id);
alter table sale add constraint FKonrcqwf09u6spb6ty6sh11jh5 foreign key (product_id) references product (id);
